// global variables spread sheet ID
var ssID = '1gdqWUIXU0C5EpoEW6s9ljzkTZmmMvDls6uGXLmPG_Og';
var ss = SpreadsheetApp.openById(ssID);
var wsAccountGroup = ss.getSheetByName('account_group');
var wsCurrency = ss.getSheetByName('currency');
var wsAccount = ss.getSheetByName('account');
var wsCategory = ss.getSheetByName('category');
var wsVendor = ss.getSheetByName('vendor');
var wsTransaction = ss.getSheetByName('transaction');
var wsSettings = ss.getSheetByName('settings');
var Route = {};

// ------------------------------------------ ROUTE PAGES-----------------------------------------
// function to open different html files
Route.path = function (route, callback) {
  Route[route] = callback;
};

// doGet function that is called at first
function doGet(request) {
  Route.path('dashboard', loadDashboard);
  Route.path('account', loadAccount);
  Route.path('agroup', loadAccountGroup);
  Route.path('currency', loadCurrency);
  Route.path('transactions', loadTransactions);
  if (Route[request.parameters.page]) {
    return Route[request.parameters.page]();
  }
  return Route['dashboard']();
}

// render function to open each html files with relevant headers
function render(file, argsObject) {
  var template = HtmlService.createTemplateFromFile(file);
  if (argsObject) {
    var keys = Object.keys(argsObject);
    keys.forEach(function (key) {
      template[key] = argsObject[key];
    });
  }
  return template.evaluate().setXFrameOptionsMode(HtmlService.XFrameOptionsMode.ALLOWALL);
}

// ------------------------------------------- LOAD PAGES-----------------------------------------
// load dashboard page
function loadDashboard() {
  return render('dashboard');
}

// load Account Groups page
function loadAccountGroup() {
  return render('agroup');
}
function loadCurrency() {
  return render('currency');
}

// load Transactions page
function loadTransactions() {
  var accountData = getSheetData(wsAccount, 2, 2);
  var currencyData = getSheetData(wsCurrency, 2, 2);
  var categoryData = getSheetData(wsCategory, 2, 2);
  var vendorData = getSheetData(wsVendor, 2, 2);
  var transactionData = getSheetData(wsTransaction, 2, 1);
  var settingsData = getSettings();
  return render('transactions', {
    account: accountData,
    currency: currencyData,
    category: categoryData,
    vendor: vendorData,
    transaction: transactionData,
    settings: settingsData
  });
}

// load account page
function loadAccount() {
  // var obj = getDropDownLists();
  // return render('account', { account_group: obj[0], base_currency: obj[1] });
  return render('account');
}

// ------------------------------------------- GET VALUES FROM SHEETS -----------------------------------------
// function to get currency codes from the currency sheet
function getSheetData(sheetName, startRow, startCol) {
  var listResult = JSON.stringify(sheetName.getRange(startRow, startCol, sheetName.getLastRow() - 1, sheetName.getLastColumn() - 1).getValues());
  return listResult;
}

function getSettings() {
  var settingsObj = {};
  var listSettings = wsSettings.getRange(1, 1, wsSettings.getLastRow(), 2).getValues();
  for (let i = 0; i < listSettings.length; i++) {
    var key = listSettings[i][0];
    var val = listSettings[i][1];
    settingsObj[key] = val;
  }
  return JSON.stringify(settingsObj);
}
