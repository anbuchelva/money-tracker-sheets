/* @Include JavaScript and CSS Files */
function include(filename) {
  return HtmlService.createHtmlOutputFromFile(filename).getContent();
}

function addExpense({
  e_date,
  e_account,
  e_category,
  e_subcategory,
  e_vendor,
  e_notes,
  e_description,
  et_currency,
  er_currency,
  ew_currency,
  efx_t_r,
  efx_r_w,
  et_value,
  er_value,
  ew_value
}) {
  var wsTransaction = ss.getSheetByName('transaction');
  wsTransaction.appendRow([
    Math.random().toString(36).slice(2, 10).toUpperCase(),
    e_date,
    e_account,
    e_category,
    e_subcategory,
    'Expense',
    e_vendor,
    e_notes,
    e_description,
    et_currency,
    er_currency,
    ew_currency,
    efx_t_r,
    efx_r_w,
    et_value * -1,
    er_value * -1,
    ew_value * -1,
    '',
    '',
    '',
    '',
    new Date()
  ]);
  updateFormulaTransactions();
}

function addIncome({
  i_date,
  i_account,
  i_category,
  i_subcategory,
  i_notes,
  i_description,
  it_currency,
  ir_currency,
  iw_currency,
  ifx_t_r,
  ifx_r_w,
  it_value,
  ir_value,
  iw_value
}) {
  var wsTransaction = ss.getSheetByName('transaction');
  wsTransaction.appendRow([
    Math.random().toString(36).slice(2, 10).toUpperCase(),
    i_date,
    i_account,
    i_category,
    i_subcategory,
    'Income',
    '',
    i_notes,
    i_description,
    it_currency,
    ir_currency,
    iw_currency,
    ifx_t_r,
    ifx_r_w,
    it_value,
    ir_value,
    iw_value,
    '',
    '',
    '',
    '',
    new Date()
  ]);
  updateFormulaTransactions();
}

function addTransfer({
  t_date,
  ft_account,
  tt_account,
  t_notes,
  t_description,
  tt_currency,
  tr_currency,
  ftw_currency,
  ttw_currency,
  ftfx_t_r,
  ttfx_t_r,
  ftfx_r_w,
  ttfx_r_w,
  tt_value,
  ftr_value,
  ttr_value,
  ftw_value,
  ttw_value
}) {
  var wsTransaction = ss.getSheetByName('transaction');
  var transactionID = Math.random().toString(36).slice(2, 10).toUpperCase();
  var currentDate = new Date();
  wsTransaction.appendRow([
    transactionID,
    t_date,
    ft_account,
    '',
    '',
    'Transfer Out',
    '',
    t_notes,
    t_description,
    tt_currency,
    tr_currency,
    ftw_currency,
    ftfx_t_r,
    ftfx_r_w,
    tt_value * -1,
    ftr_value * -1,
    ftw_value * -1,
    tt_account,
    '',
    '',
    '',
    currentDate
  ]);
  updateFormulaTransactions();
  wsTransaction.appendRow([
    transactionID,
    t_date,
    tt_account,
    '',
    '',
    'Transfer In',
    '',
    t_notes,
    t_description,
    tt_currency,
    tr_currency,
    ttw_currency,
    ttfx_t_r,
    ttfx_r_w,
    tt_value,
    ttr_value,
    ttw_value,
    ft_account,
    '',
    '',
    '',
    currentDate
  ]);
  updateFormulaTransactions();
}
function updateFormulaTransactions() {
  transLastRow = wsTransaction.getLastRow();
  Logger.log(transLastRow);
  wsTransaction.getRange(transLastRow, 19).setFormulaR1C1('=month(R[0]C[-17])');
  wsTransaction.getRange(transLastRow, 20).setFormulaR1C1('=year(R[0]C[-18])');
  wsTransaction.getRange(transLastRow, 21).setFormulaR1C1('=SUMIF(R2C3:R[0]C3,R[0]C3,R2C16:R[0]C16)');
}
